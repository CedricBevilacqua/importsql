import sqlite3
from csvActions import *

connection = sqlite3.connect('basededonnees.sqlite3')
cursor=connection.cursor()

with open('dataToAdd.csv') as csvfile:
    reader = csv.DictReader(csvfile, delimiter=';')
    reference = []
    nom = []
    prix = []
    date_time = []
    liste = []
    for row in reader:
        liste.append(row['ref'])
        liste.append(row['name'])
        liste.append(row['price'])
        liste.append(row['release-date'])
        cursor.execute("SELECT reference FROM csv WHERE reference=?", (row['ref'], ))

        if cursor.fetchone() == None:
            cursor.execute('INSERT INTO csv(reference, nom, prix, date_time) VALUES (?,?,?,?)', liste)
        else:
            cursor.execute('UPDATE csv SET nom=?, prix=?, date_time=? WHERE reference=?', (row['name'], row['price'], row['release-date'], row['ref'], ))

        liste.clear()
            
connection.commit()
import unittest
from csvActions import *

class test_csv(unittest.TestCase):
    def test_readCSV(self):
        fileTestContent = [["ref.","name","price","release-date"], ["IEL-51619","Ishtar - Les Jardins De Babylone","40.0","2019-10-04"], ["BLU-KIB01","Kingdomino","19.90","2016-10-21"], ["B0-DRA01","Draftosaurus","19.90","2019-02-22"], ["FLY004JU","Jurassic Snack","19.90","2018-04-06"]]
        self.assertEqual(readCSV("tests/testfile.csv","|"), fileTestContent)
    
    def test_writeCSV(self):
        self.assertTrue(writeCSV("tests/testfile.csv", readCSV("tests/testfile.csv","|"), ";"))
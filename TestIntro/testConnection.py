import sqlite3
connection = sqlite3.connect('test_baseDeDonnees.sqlite3')
cursor=connection.cursor()

purchases = [
                (50, 'BUY', 'IBM', '1000', '0756985412'),
                (100, 'BUY', 'MSFT', '1000', '0659654785'),
                (586, 'SELL', 'IBM', '500', '0658985741'),
            ]

cursor.executemany('INSERT INTO contacts VALUES (?,?,?,?,?)', purchases)

connection.commit()

for row in cursor.execute("SELECT * FROM contacts"):
    print(row)
import csv
import os
import datetime

def readCSV(fileName, delimiteur):
    fileContent = ""
    with open(fileName) as csvfile:
        reader = csv.reader(csvfile, delimiter=delimiteur)
        fileContent = [row for row in reader]
    return fileContent

def writeCSV(fileName, data, delimiteur):
    returnValue = False
    fileNewName = fileName[:len(fileName)-3] + "new.csv"
    with open(fileNewName, 'w') as csvfile:
        writer = csv.writer(csvfile, delimiter=delimiteur)
        writer.writerows(data)
        returnValue = True
    return returnValue
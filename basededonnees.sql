CREATE TABLE csv(
    reference TEXT NOT NULL PRIMARY KEY,
    nom TEXT NOT NULL,
    prix FLOAT,
    date_time DATE
);